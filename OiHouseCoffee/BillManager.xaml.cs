﻿using BusinessLayer.Models;
using DataLayer.Filter;
using OiHouseCoffee.Data_Layer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for BillManager.xaml
    /// </summary>
    public partial class BillManager : Page
    {
        private readonly IBillRepository billRepository;
        private readonly IOrderInfoRepository orderInfoRepository;
        private readonly IAccountRepository accountRepository;
        private static readonly Regex _regex = new Regex("[^0-9.-]+");
        public BillManager(IBillRepository _billRepository, IOrderInfoRepository _orderInfoRepository)
        {
            InitializeComponent();
            this.billRepository = _billRepository;
            this.orderInfoRepository = _orderInfoRepository;

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            listView.ItemsSource = billRepository.GetAll();
        }

        private void ListViewItem_Selected(object sender, RoutedEventArgs e)
        {

        }
        private void ListView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ListView? listView = sender as ListView;
            GridView? gridView = listView != null ? listView.View as GridView : null;

            var width = listView != null ? listView.ActualWidth - SystemParameters.VerticalScrollBarWidth : this.Width;

            var column1 = 0.05;
            var column2 = 0.2;
            var column3 = 0.1;
            var column4 = 0.2;
            var column5 = 0.2;
            var column6 = 0.2;


            if (gridView != null && width >= 0)
            {
                gridView.Columns[0].Width = width * column1;
                gridView.Columns[1].Width = width * column2;
                gridView.Columns[2].Width = width * column3;
                gridView.Columns[3].Width = width * column4;
                gridView.Columns[4].Width = width * column5;
                gridView.Columns[5].Width = width * column6;

            }
        }

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {
            listView.ItemsSource = billRepository.GetAll();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            DateTime? startDate = StartDate.SelectedDate == null ? null : StartDate.SelectedDate.Value.Date;
            DateTime? endDate = EndDate.SelectedDate == null ? null : EndDate.SelectedDate.Value.Date;
            BillFilter orderFilter = new BillFilter();
            orderFilter.StartDate = startDate;
            orderFilter.EndDate = endDate;
            listView.ItemsSource = billRepository.FindAllByStartTimeAndEndTime(orderFilter);
        }
        private void Get_Detail(object sender, RoutedEventArgs e)
        {
            Bill bill = listView.SelectedItems.Cast<Bill>().SingleOrDefault();
            BillDetailInfo billDetailInfo = new BillDetailInfo(bill,orderInfoRepository);
            billDetailInfo.Show();
        }
    }
}
