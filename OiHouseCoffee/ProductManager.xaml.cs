﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using OiHouseCoffee.Data_Layer.Repository;
using BusinessLayer.Models;
using DataLayer.Filter;
using System.Windows.Media;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for ProductManager.xaml
    /// </summary>
    public partial class ProductManager : Page
    {
        private readonly IProductRepository productRepository;
        private readonly ICategoryRepository categoryRepository;
        private static readonly Regex _regex = new Regex("[^0-9.-]+");
        public ProductManager(IProductRepository _productRepository, ICategoryRepository _categoryRepository)
        {
            InitializeComponent();
            this.productRepository = _productRepository;
            this.categoryRepository = _categoryRepository;
            this.listView.SelectionChanged += ListView_SelectionChanged;

        }


        private void ListView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ListView? listView = sender as ListView;
            GridView? gridView = listView != null ? listView.View as GridView : null;

            var width = listView != null ? listView.ActualWidth - SystemParameters.VerticalScrollBarWidth : this.Width;

            var column1 = 0.2;
            var column2 = 0.2;
            var column3 = 0.2;
            var column4 = 0.2;
            var column5 = 0.2;

            if (gridView != null && width >= 0)
            {
                gridView.Columns[0].Width = width * column1;
                gridView.Columns[1].Width = width * column2;
                gridView.Columns[2].Width = width * column3;
                gridView.Columns[3].Width = width * column4;
                gridView.Columns[4].Width = width * column5;

            }
        }

        private void Set_ItemComboBox()
        {
            List<FoodCategory> productCategory = (List<FoodCategory>)categoryRepository.GetAll();
            comboBoxCategory.SelectedIndex = 0;
            ComboBoxItem comboBoxItemSelected = (ComboBoxItem)comboBoxCategory.SelectedItem;
            foreach (FoodCategory category in productCategory)
            {
                comboBoxCategory.Items.Add(category);
            }
            comboBoxCategory.DisplayMemberPath = "Name";
            comboBoxCategory.SelectedValuePath = "Id";
            
            comboBoxCategory.Text = comboBoxItemSelected.Content.ToString();

        }


        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            listView.ItemsSource = productRepository.List();
            Set_ItemComboBox();
        }

        private void Open_Add(object sender, RoutedEventArgs e)
        {
            AddProduct addProduct = new AddProduct(productRepository, null, categoryRepository, listView);
            addProduct.Show();
        }
       

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int count = listView.SelectedItems.Count;
            if (count > 0)
            {
                btnEdit.IsEnabled = true;
            }
            else
            {
                btnEdit.IsEnabled = false;

                btnEdit.Background = new SolidColorBrush(Colors.Gray);
            }
        }

        private void comboBoxCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(comboBoxCategory.SelectedIndex == 0)
            {
                ComboBoxItem comboBoxItemSelected = (ComboBoxItem)comboBoxCategory.SelectedItem;
                comboBoxCategory.SelectedIndex = 0;
                comboBoxCategory.Text = "All category";
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Do you wan't remove Product?", "Remove product", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                List<Food> foods = listView.SelectedItems.Cast<Food>().ToList();
                foods.ForEach(foods => productRepository.Remove(foods));
                listView.ItemsSource = productRepository.List();
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            int count = listView.SelectedItems.Count;
            if (count > 0)
            {
                List<Food> products = listView.SelectedItems.Cast<Food>().ToList();
                products.ForEach(product =>
                {
                    AddProduct addProduct = new AddProduct(productRepository, product, categoryRepository , listView);
                    addProduct.Show();
                });
            }
            else
            {
                MessageBox.Show("Plase select member");
            }
        }

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {
            listView.ItemsSource = productRepository.List();
            searchById.Text = null;
            searchByPrice.Text = null;
            comboBoxCategory.SelectedIndex = 0;
            ComboBoxItem comboBoxItemSelected = (ComboBoxItem)comboBoxCategory.SelectedItem;
            comboBoxCategory.Text = comboBoxItemSelected.Content.ToString();
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            int? id = !String.IsNullOrEmpty(searchById.Text) ? int.Parse(searchById.Text) : null;
            string? name = searchByName.Text;
            int? price = !String.IsNullOrEmpty(searchByPrice.Text) ? int.Parse(searchByPrice.Text) : null;
            String idCategory = comboBoxCategory.SelectedValue != null ? comboBoxCategory.SelectedValue.ToString() : null;
            ProductFilter productFilter = new ProductFilter();
            productFilter.Id = id;
            productFilter.ProductName = name;
            productFilter.Price = price;
            productFilter.CategoryId = !String.IsNullOrEmpty(idCategory) ? int.Parse(idCategory) : null;
            listView.ItemsSource = productRepository.FindAllBy(productFilter);
        }

        private void Button_Add(object sender, RoutedEventArgs e)
        {

        }

        private void searchById_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_regex.IsMatch(searchById.Text))
            {
                MessageBox.Show("Plase input number");
                searchById.Text = string.Empty;
            }
        }


        private void searchByPrice_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_regex.IsMatch(searchByPrice.Text))
            {
                MessageBox.Show("Plase input number");
                searchByPrice.Text = string.Empty;
            }
        }

        private void ListViewItem_Selected(object sender, RoutedEventArgs e)
        {

        }
    }
}
