﻿using BusinessLayer.Models;
using DataLayer.Filter;
using Microsoft.EntityFrameworkCore.Metadata;
using OiHouseCoffee.Data_Layer.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OiHouseCoffee
{
   
    public partial class EmployeeManeger : Page
    {
        private readonly IAccountRepository accountRepository;
        private static readonly Regex _regex = new Regex("[^0-9.-]+");
        public EmployeeManeger(IAccountRepository _accountRepository)
        {
            InitializeComponent();
            this.accountRepository = _accountRepository;
            this.listView.SelectionChanged += ListView_SelectionChanged;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            listView.ItemsSource = accountRepository.GetAll();
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int count = listView.SelectedItems.Count;
            if (count > 0)
            {
                btnEdit.IsEnabled = true;
            }
            else
            {
                btnEdit.IsEnabled = false;
                btnEdit.Background = new SolidColorBrush(Colors.Gray);
            }
        }
        private void ListViewItem_Selected(object sender, RoutedEventArgs e)
        {

        }
        private void ListView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ListView? listView = sender as ListView;
            GridView? gridView = listView != null ? listView.View as GridView : null;

            var width = listView != null ? listView.ActualWidth - SystemParameters.VerticalScrollBarWidth : this.Width;

            var column1 = 0.2;
            var column2 = 0.2;
            var column3 = 0.2;
            var column4 = 0.2;
            var column5 = 0.2;
            var column6 = 0.2;


            if (gridView != null && width >= 0)
            {
                gridView.Columns[0].Width = width * column1;
                gridView.Columns[1].Width = width * column2;
                gridView.Columns[2].Width = width * column3;
                gridView.Columns[3].Width = width * column4;
                gridView.Columns[4].Width = width * column5;
                gridView.Columns[5].Width = width * column6;

            }
        }
        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int? id = !String.IsNullOrEmpty(searchById.Text) ? int.Parse(searchById.Text) : null;
            string email = searchByEmail.Text;
            string userName = searchByName.Text;
            string phoneNumber = searchByPhoneNumber.Text;
            AccountFilter accountFilter = new AccountFilter();
            accountFilter.AccountId = id;
            accountFilter.Email = email;
            accountFilter.PhoneNumber = phoneNumber;
            accountFilter.UserName = userName;
            listView.ItemsSource = accountRepository.FindAllBy(accountFilter);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            AddEmployee addEmployee = new AddEmployee(this, null, accountRepository, listView);
            addEmployee.Show();
        }

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {

            AccountFilter accountFilter = new AccountFilter();
            listView.ItemsSource = accountRepository.GetAll();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Do you wan't remove Account?", "Remove Account", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                List<Account> members = listView.SelectedItems.Cast<Account>().ToList();
                foreach (Account member in members)
                {
                    member.Status = false;
                    accountRepository.Update(member);
                }
                listView.ItemsSource = accountRepository.GetAll();
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            int count = listView.SelectedItems.Count;
            if (count > 0)
            {
                List<Account> products = listView.SelectedItems.Cast<Account>().ToList();
                products.ForEach(member =>
                {
                    AddEmployee addEmployee = new AddEmployee(this, member, accountRepository,listView);
                    addEmployee.Show();
                });
            }
            else
            {
                MessageBox.Show("Plase select member");
            }
        }


        private void searchById_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            if (_regex.IsMatch(searchById.Text))
            {
                MessageBox.Show("Plase input number");
                searchById.Text = string.Empty;
            }
        }

        private void searchByPhoneNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_regex.IsMatch(searchById.Text))
            {
                MessageBox.Show("Plase input number");
                searchById.Text = string.Empty;
            }
        }
    }
}
