﻿using BusinessLayer.Models;
using DataLayer.Filter;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Data_Layer;
using OiHouseCoffee.Data_Layer.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Window
    {
        private readonly IProductRepository productRepository;
        private readonly IBillRepository orderRepository;
        private readonly IAccountRepository memberRepository;
        private readonly ICategoryRepository categoryRepository;
        private readonly IOrderInfoRepository orderInfoRepository;
        private Account account;
        private readonly MainWindow mainWindow;
        private static readonly Regex _regex = new Regex("[^0-9.-]+");
        List<ProductCart> listProductCart = new List<ProductCart>();
        OrderInfoDao orderInfoDao;


        private int totalPrice;

        public Home(MainWindow _mainWindow, IProductRepository _productRepository, IBillRepository _orderRepository, IAccountRepository _memberRepository,
            ICategoryRepository _categoryRepository,Account _account, IOrderInfoRepository _orderInfoRepository)
        {
            InitializeComponent();
            Closing += Home_Closing;
            this.mainWindow = _mainWindow;
            this.productRepository = _productRepository;
            this.orderRepository = _orderRepository;
            this.memberRepository = _memberRepository;
            ListProduct.ItemsSource = productRepository.List();
            ListCart.ItemsSource = listProductCart;
            this.categoryRepository = _categoryRepository;
            account = _account;
            Set_ItemComboBox();
            setTotal();
            setTotalCheckOut();
            this.orderInfoRepository = _orderInfoRepository;
        }


        private void Set_ItemComboBox()
        {
            List<FoodCategory> productCategory = (List<FoodCategory>)categoryRepository.GetAll();
            comboBoxCategory.SelectedIndex = 0;
            ComboBoxItem comboBoxItemSelected = (ComboBoxItem)comboBoxCategory.SelectedItem;
            foreach (FoodCategory category in productCategory)
            {
                comboBoxCategory.Items.Add(category);
            }
            comboBoxCategory.DisplayMemberPath = "Name";
            comboBoxCategory.SelectedValuePath = "Id";

            comboBoxCategory.Text = comboBoxItemSelected.Content.ToString();

        }


        private void Home_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            mainWindow.Show();
        }

        private void comboBoxCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ProductFilter productFilter = new ProductFilter();
            String idCategory = null;
            if (comboBoxCategory.SelectedIndex < 1)
            {
                ComboBoxItem comboBoxItemSelected = (ComboBoxItem)comboBoxCategory.SelectedItem;
                comboBoxCategory.SelectedIndex = 0;
                comboBoxCategory.Text = "All category";
            }
            else
            {
                idCategory = comboBoxCategory.SelectedValue.ToString();
            }
            productFilter.ProductName = searchByName.Text;
            productFilter.CategoryId = !String.IsNullOrEmpty(idCategory) ? int.Parse(idCategory) : null;
            ListProduct.ItemsSource = productRepository.FindAllBy(productFilter);
        }
        private void searchByName_TextChanged(object sender, TextChangedEventArgs e)
        {
            ProductFilter productFilter = new ProductFilter();
            String idCategory = comboBoxCategory.SelectedValue != null ? comboBoxCategory.SelectedValue.ToString() : null;
            productFilter.ProductName = searchByName.Text;
            productFilter.CategoryId = !String.IsNullOrEmpty(idCategory) ? int.Parse(idCategory) : null;
            ListProduct.ItemsSource = productRepository.FindAllBy(productFilter);
        }

        private void Control_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListView lv = sender as ListView;
            Food food = lv.SelectedItems.Cast<Food>().FirstOrDefault();
            ProductCart item = new ProductCart(food, 1);
            int index = listProductCart.FindIndex(x => x.product.Id == item.product.Id);
            if (index >= 0)
            {
                listProductCart[index].quantity++;
                listProductCart[index].setPrice();
            }
            else
            {
                listProductCart.Add(item);
            }
            ListCart.ItemsSource = new ObservableCollection<ProductCart>(listProductCart);
            CollectionViewSource.GetDefaultView(ListCart.ItemsSource).Refresh();
            setTotal();
            setTotalCheckOut();

        }

        private void Button_Add_Click(object sender, RoutedEventArgs e)
        {
            ProductCart item = ListCart.SelectedItems.Cast<ProductCart>().FirstOrDefault();
            int index = listProductCart.FindIndex(x => x.product.Id == item.product.Id);
            listProductCart[index].quantity++;
            listProductCart[index].setPrice();
            ListCart.SelectedItem = item;
            ListCart.ItemsSource = new ObservableCollection<ProductCart>(listProductCart);
            CollectionViewSource.GetDefaultView(ListCart.ItemsSource).Refresh();
            setTotal();
            setTotalCheckOut();
        }
        private void Button_Remove_Click(object sender, RoutedEventArgs e)
        {
            ProductCart item = ListCart.SelectedItems.Cast<ProductCart>().FirstOrDefault();
            int index = listProductCart.FindIndex(x => x.product.Id == item.product.Id);
            listProductCart[index].quantity--;
            if(listProductCart[index].quantity == 0)
            {
                listProductCart.RemoveAt(index);
                ListCart.ItemsSource = new ObservableCollection<ProductCart>(listProductCart);
                CollectionViewSource.GetDefaultView(ListCart.ItemsSource).Refresh();
            }
            else
            {
                listProductCart[index].setPrice();
                ListCart.SelectedItem = item;
                ListCart.ItemsSource = new ObservableCollection<ProductCart>(listProductCart);
                CollectionViewSource.GetDefaultView(ListCart.ItemsSource).Refresh();
            }
            setTotal();
            setTotalCheckOut();
        }

        private void Quantity_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBoxQuantity = sender as TextBox;
            if(!String.IsNullOrEmpty(textBoxQuantity.Text))
            {
                int quantity = int.Parse(textBoxQuantity.Text);
                ProductCart item = ListCart.SelectedItems.Cast<ProductCart>().FirstOrDefault();
                int index = listProductCart.FindIndex(x => x.product.Id == item.product.Id);
                listProductCart[index].setPrice(quantity);
                ListCart.SelectedItem = item;
                ListCart.ItemsSource = new ObservableCollection<ProductCart>(listProductCart);
                CollectionViewSource.GetDefaultView(ListCart.ItemsSource).Refresh();
                setTotal();
                setTotalCheckOut();
            }
            else
            {
                
                MessageBox.Show("Nếu bạn muốn xóa sản phẩm khỏi giỏ hàng hãy nhập số 0. Hoặc số sản phẩm bạn muốn lớn hơn 0.");
                ProductCart item = ListCart.SelectedItems.Cast<ProductCart>().FirstOrDefault();
                int index = listProductCart.FindIndex(x => x.product.Id == item.product.Id);
                textBoxQuantity.Text = listProductCart[index].quantity.ToString();
            }
            
        }

        public void setTotal()
        {
            int total = 0;
            foreach(var item in listProductCart)
            {
                total+= item.quantity * item.product.Price;
            }
            TotalBeforeDiscount.Content = total.ToString();
        }

        public void setTotalCheckOut()
        {
            int total = int.Parse(TotalBeforeDiscount.Content.ToString());
            if (!String.IsNullOrEmpty(Discount.Text))
            {
                int discount = int.Parse(Discount.Text);
                totalCheckout.Content = total * (100 - discount) / 100;
            }
            else
            {
                totalCheckout.Content = total;
            }
            
            

        }

        private void Discount_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TotalBeforeDiscount.Content != null)
            {
                int total = int.Parse(TotalBeforeDiscount.Content.ToString());
                if (!String.IsNullOrEmpty(Discount.Text))
                {
                    if (_regex.IsMatch(Discount.Text) || 0 > int.Parse(Discount.Text)
                    || int.Parse(Discount.Text) > 100)
                    {
                        totalCheckout.Content = total;
                        Discount.Text = null;
                        MessageBox.Show("Vui lòng nhập số lớn hơn 0 và nhỏ hơn 100 hoặc không để trống.");
                    }
                    else
                    {
                        int discount = int.Parse(Discount.Text);
                        totalCheckout.Content = total * (100 - discount) / 100;
                    }
                }
                else
                {
                    totalCheckout.Content = total;
                }
            }
            else
            {
                if(!String.IsNullOrEmpty(Discount.Text))
                {
                    if (_regex.IsMatch(Discount.Text) || 0 > int.Parse(Discount.Text)
                    || int.Parse(Discount.Text) > 100)
                    {
                        Discount.Text = null;
                        MessageBox.Show("Vui lòng nhập số lớn hơn 0 và nhỏ hơn 100 hoặc không để trống.");
                    }
                }
                
            }
        }

        private void Check_Out(object sender, RoutedEventArgs e)
        {
            if(listProductCart.Count == 0)
            {
                MessageBox.Show("Vui lòng gọi đồ trước khi checkout!");
            }
            else
            {
                int discount = 0;
                int totalAfterDiscount = int.Parse(totalCheckout.Content.ToString());
                if (!String.IsNullOrEmpty(Discount.Text.ToString()))
                {
                    discount = int.Parse(Discount.Text.ToString());
                }
                int totalBeforeDiscount = int.Parse(TotalBeforeDiscount.Content.ToString());
                Bill bill = new Bill();
                bill.Discount = discount;
                bill.DateCheckOut = DateTime.Now;
                bill.DateCheckIn = DateTime.Now;
                bill.IdAccount = account.Id;
                bill.total = totalAfterDiscount;
                orderRepository.Add(bill);
                foreach (ProductCart item in listProductCart)
                {
                    BillInfo billInfo = new BillInfo();
                    billInfo.IdBill = bill.Id;
                    billInfo.IdFood = item.product.Id;
                    billInfo.Count = item.quantity;
                    billInfo.price = item.price;
                    orderInfoRepository.Add(billInfo);
                }
                BillDetail billDetail = new BillDetail(listProductCart, account, totalAfterDiscount, discount, totalBeforeDiscount,this, bill.Id);
                billDetail.Show();
            }
            
        }
        public void reload()
        {
            ListProduct.ItemsSource = productRepository.List();
            ListCart.ItemsSource = null;
            totalCheckout.Content = null;
            TotalBeforeDiscount.Content = null;
            Discount.Text = null;
            searchByName.Text = null;
            Set_ItemComboBox();
        }
    }
}
