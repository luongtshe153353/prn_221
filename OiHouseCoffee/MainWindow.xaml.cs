﻿using Microsoft.Extensions.Configuration;
using OiHouseCoffee.Data_Layer.Repository;
using System;
using System.Windows;
using System.Windows.Controls;


namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IProductRepository productRepository;
        private readonly IAccountRepository accountRepository;
        private readonly IBillRepository billRepository;
        private readonly ICategoryRepository categoryRepository;
        private readonly IOrderInfoRepository orderInfoRepository;


        public MainWindow(IProductRepository _productRepository, IAccountRepository _accountRepository, IBillRepository _billRepository, 
            ICategoryRepository _categoryRepository, IOrderInfoRepository orderInfoRepository)
        {

            InitializeComponent();
            this.productRepository = _productRepository;
            this.accountRepository = _accountRepository;
            this.billRepository = _billRepository;
            this.categoryRepository = _categoryRepository;
            this.orderInfoRepository = orderInfoRepository;
        }

        public void resetFormLogin()
        {
            textBoxUserName.Text = null;
            textBoxPassWord.Password = null;
        }
        private void textBoxUserName_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string username = textBoxUserName.Text.ToString();
            string password = textBoxPassWord.Password.ToString();
            var account = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("account");
            if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
            {
                if (username.Equals(account["username"]) && password.Equals(account["password"]))
                {
                    Session.Username = username;
                    this.Hide();
                    AdminManager adminManager = new AdminManager(this, productRepository, accountRepository, billRepository,categoryRepository,orderInfoRepository);
                    adminManager.Show();
                    resetFormLogin();
                }
                else if (accountRepository.FindByPasswordAndUserName(password, username) != null && accountRepository.FindByPasswordAndUserName(password, username).Status == true)
                {
                    Session.Username = username;
                    this.Hide();
                    Home home = new Home(this, productRepository, billRepository, accountRepository, categoryRepository, accountRepository.FindByPasswordAndUserName(password, username), orderInfoRepository);
                    home.Show();
                    resetFormLogin();
                }
                else
                {
                    MessageBox.Show("The username or password is incorrect");
                }
            }
            else
            {
                MessageBox.Show("Please enter username and password");
            }
        }
    }
}
