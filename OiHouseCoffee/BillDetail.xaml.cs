﻿using BusinessLayer.Models;
using OiHouseCoffee.Business_Layer.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.Json;
using System.Text.Json.Serialization;
using RestSharp;
using System.Drawing;
using System.Runtime.Intrinsics.X86;


namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for BillDetail.xaml
    /// </summary>
    public partial class BillDetail : Window
    {
        private List<ProductCart> productCarts;
        private Account account;
        private int totalCheckout;
        private int discount;
        private int totalBeforDiscount;
        private readonly Home home;
        private long billId;
        public BillDetail(List<ProductCart> _productCarts,Account _account,int _totalCheckout,int _discount, int _totalBeforDiscount, Home _home, long _billId )
        {
            InitializeComponent();
            productCarts = _productCarts;
            account = _account;
            discount = _discount;
            totalBeforDiscount = _totalBeforDiscount;
            totalCheckout = _totalCheckout;
            billId = _billId;
            ListProduct.Items.Clear();
            ListProduct.ItemsSource = productCarts;
            setHeight();
            setInfo();
            createQr();
            this.home = _home;
            Closing += BillDetail_Closing;
        }

        public void setHeight()
        {
            double desiredHeight = productCarts.Count*30;
            ListProduct.Height = desiredHeight;
            Height = desiredHeight + 720;
        }

        public void setInfo()
        {
            txtAccount.Text = account.UserName;
            timeCheckIn.Text = DateTime.Now.ToString();
            timeCheckOut.Text = DateTime.Now.ToString();
            TotalBeforDiscount.Text = totalBeforDiscount.ToString();
            PercentDiscount.Text = discount.ToString();
            TotalAfterDiscount.Text = totalCheckout.ToString();
            Discount.Text = (totalCheckout - totalBeforDiscount).ToString();
        }

        public void createQr()
        {
            var apiRequest = new ApiRequest();
            apiRequest.acqId = 970415;
            apiRequest.accountNo = 104872819334;
            apiRequest.accountName = "Tong Sy Luong";
            apiRequest.amount = Convert.ToInt32(totalCheckout.ToString());
            apiRequest.format = "text";
            apiRequest.template = "qr_only";
            apiRequest.addInfo = "Ma hoa don: " + billId;
            var jsonRequest = JsonSerializer.Serialize<ApiRequest>(apiRequest);
            // use restsharp for request api.
            var client = new RestClient("https://api.vietqr.io/v2/generate");
            var request = new RestRequest();

            request.Method = Method.Post;
            request.AddHeader("Accept", "application/json");

            request.AddParameter("application/json", jsonRequest, ParameterType.RequestBody);

            var response = client.Execute(request);
            var content = response.Content;
            var dataResult = JsonSerializer.Deserialize<ApiResponse>(content);

            string base64String = dataResult.data.qrDataURL.Replace("data:image/png;base64,", "");
            byte[] imageBytes = Convert.FromBase64String(base64String);
            vietQR.Source = ByteToImage(imageBytes);
            
        }

        public static ImageSource ByteToImage(byte[] imageData)
        {
            BitmapImage biImg = new BitmapImage();
            MemoryStream ms = new MemoryStream(imageData);
            biImg.BeginInit();
            biImg.StreamSource = ms;
            biImg.EndInit();

            ImageSource imgSrc = biImg as ImageSource;

            return imgSrc;
        }

        private void BillDetail_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            home.reload();
        }

    }
}
