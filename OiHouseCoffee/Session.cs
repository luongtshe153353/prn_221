﻿
using BusinessLayer.Models;
using System.Collections.Generic;


namespace OiHouseCoffee
{
    internal class Session
    {
        public static string? Username { get; set; } = null;
        public static List<BillInfo> carts { get; set; } = null;
    }
}
