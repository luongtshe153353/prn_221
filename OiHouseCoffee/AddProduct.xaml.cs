﻿using BusinessLayer.Models;
using DataLayer.Filter;
using Microsoft.Win32;
using OiHouseCoffee.Data_Layer.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Metrics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for AddProduct.xaml
    /// </summary>
    public partial class AddProduct : Window
    {
        private readonly IProductRepository productRepository;
        private readonly ICategoryRepository categoryRepository;
        private ListView listView;
        private Food? product;
        private static readonly Regex _regex = new Regex("[^0-9.-]+");

        public AddProduct(IProductRepository _productRepository, Food? _product, ICategoryRepository _categoryRepository, ListView _listView)
        {
            InitializeComponent();
            this.productRepository = _productRepository;
            this.product = _product;
            this.categoryRepository = _categoryRepository;
            this.listView = _listView;
        }

        private void Set_ItemComboBox()
        {
            List<FoodCategory> productCategory = (List<FoodCategory>)categoryRepository.GetAll();
            comboBoxCategory.SelectedIndex = 0;
            ComboBoxItem comboBoxItemSelected = (ComboBoxItem)comboBoxCategory.SelectedItem;
            foreach (FoodCategory category in productCategory)
            {
                comboBoxCategory.Items.Add(category);
            }
            comboBoxCategory.DisplayMemberPath = "Name";
            comboBoxCategory.SelectedValuePath = "Id";

            comboBoxCategory.Text = comboBoxItemSelected.Content.ToString();

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Set_ItemComboBox();
            if (product != null)
            {
                txtBoxName.Text = product.Name;
                txtBoxPrice.Text = product.Price.ToString();
                comboBoxCategory.SelectedValue = product.IdCategory;
                if( product.Image == null )
                {
                    ImageSourceConverter imgsc = new ImageSourceConverter();
                    string path = @"Images\Logo.JPG";
                    ImageSource imageSource = (ImageSource)imgsc.ConvertFromString(path);
                }
                else
                {
                    ImageSourceConverter imgsc = new ImageSourceConverter();
                    string path = @"Images\"+product.Image;
                    ImageSource imageSource = (ImageSource)imgsc.ConvertFromString(path);
                }
                
                txtBoxId.Visibility = Visibility.Visible;
                labelId.Visibility = Visibility.Visible;
            }
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            string name = txtBoxName.Text;
            string price = txtBoxPrice.Text;
            if(comboBoxCategory.SelectedValue == null)
            {
                MessageBox.Show("Vui lòng chọn category!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string categoryId = comboBoxCategory.SelectedValue.ToString();
            Food productFindByName = productRepository.FindByName(name);
            string avatarPath;
            if (imageProduct.Tag != null)
            {              
                avatarPath = Path.GetFullPath(imageProduct.Tag.ToString());
            }
            else
            {
                avatarPath = @"Images\Logo.JPG";
            }
            Food? p = null;
            if (product != null)
            {
                p = product;
            }
            else
            {
                p = new Food();
            }
            p.Name = name;
            p.Price = int.Parse(price);
            p.IdCategory = int.Parse(categoryId);
            p.Image = avatarPath;
            if (product != null)
            {

                if (productFindByName != null && !product.Id.Equals(productFindByName.Id))
                {
                    MessageBox.Show("Name product đã tồn tại!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    productRepository.Update(p);
                    listView.ItemsSource = productRepository.List();
                    this.Close();
                }

            }
            else
            {

                if (productFindByName != null)
                {
                    MessageBox.Show("Name product đã tồn tại!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    productRepository.Add(p);
                    listView.ItemsSource = productRepository.List();
                    this.Close();
                }

            }

        }

        private void btn_Click1(object sender, RoutedEventArgs e)
        {
            //Create a new instance of openFileDialog
            OpenFileDialog image = new OpenFileDialog();
            //Filter
            image.Filter = "Image Files|*.jpg;*.jpeg;*.png;*.tif;...";

            //When the user select the file
            if (image.ShowDialog() == true)
            {
                //Get the file's path
                string imgPath = image.FileName;
                //Do something
                ImageSourceConverter imgsc = new ImageSourceConverter();
                ImageSource imageSource = (ImageSource)imgsc.ConvertFromString(imgPath);
                imageProduct.Tag = imgPath;
                imageProduct.Source = imageSource;
            }
        }

        private static String GetDestinationPath(string filename, string foldername)
        {
            String appStartPath = System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);

            appStartPath = String.Format(appStartPath + "\\{0}\\" + filename, foldername);
            return appStartPath;
        }

        private void txtBoxPrice_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_regex.IsMatch(txtBoxPrice.Text))
            {
                MessageBox.Show("Plase input number");
                txtBoxPrice.Text = string.Empty;
            }
        }


        //private void txtBoxPhoneNumber_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    if (_regex.IsMatch(txtBoxPhoneNumber.Text))
        //    {
        //        MessageBox.Show("Plase input number");
        //        txtBoxPhoneNumber.Text = string.Empty;
        //    }
        //}


    }
}
