﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Filter
{
    public class ProductFilter
    {
        public int? Id { get; set; }
        public int? CategoryId { get; set; }
        public string ProductName { get; set; }
        public int? Price { get; set; }
    }
}
