﻿using BusinessLayer.Models;
using DataLayer.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OiHouseCoffee.Data_Layer.Repository
{
    public interface IOrderInfoRepository
    {
        void Add(BillInfo billInfo);

        IEnumerable<BillInfo> FindAllById(int id);


    }
}
