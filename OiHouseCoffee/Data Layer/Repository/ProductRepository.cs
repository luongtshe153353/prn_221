﻿using BusinessLayer.Models;
using DataLayer.Filter;
using System;
using System.Collections.Generic;
using System.Linq;


namespace OiHouseCoffee.Data_Layer.Repository
{
    internal class ProductRepository : IProductRepository
    {
        public void Add(Food Food)
        {
            ProductDao.Instance.Add(Food);
        }

        public IEnumerable<Food> FindAllBy(ProductFilter filter)
        {
            if (filter != null)
            {
                return ProductDao.Instance.FindAll(product => (filter.Id == null || product.Id.Equals(filter.Id)) &&
                                                              (filter.ProductName == null || product.Name.ToLower().Contains(filter.ProductName.ToLower())) &&
                                                              (filter.Price == null || product.Price.Equals(filter.Price)) &&
                                                              (filter.CategoryId == null || product.IdCategory.Equals(filter.CategoryId)));
            }
            return List();
        }

        public Food FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Food FindByName(string name)
        {
            return ProductDao.Instance.FindOne(product => product.Name == name);
        }

        public IEnumerable<Food> FindByPrice(decimal price)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Food> FindByPrice(int price)
        {
            throw new NotImplementedException();
        }

        public Food FintByCategory(int category)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Food> List()
        {
            return ProductDao.Instance.List();
        }

        public void Remove(Food Food)
        {
            ProductDao.Instance.Delete(Food);
        }

        public void Update(Food Food)
        {
            ProductDao.Instance.Update(Food);
        }
    }
}
