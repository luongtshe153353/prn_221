﻿using DataLayer.Filter;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OiHouseCoffee.Data_Layer.Repository
{
    public interface IBillRepository
    {
        IEnumerable<Bill> GetAll();
        IEnumerable<Bill> FindAllBy(BillFilter filter);
        void Add(Bill bill);
        void Update(Bill bill);
        void Delete(Bill bill);
        Bill Get(int id);
        IEnumerable<Bill> FindAllByStartTimeAndEndTime(BillFilter filter);
        IEnumerable<Bill> FindAllByAccount(string username);
    }
}
