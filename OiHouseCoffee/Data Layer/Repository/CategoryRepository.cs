﻿using BusinessLayer.Models;
using DataLayer;
using DataLayer.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OiHouseCoffee.Data_Layer.Repository
{
    internal class CategoryRepository : ICategoryRepository
    {
        public void Add(FoodCategory foodCategory)
        {
            throw new NotImplementedException();
        }

        public Food FindById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<FoodCategory> GetAll()
        {
            return FoodCategoryDao.Instance.List();
        }

        public void Remove(FoodCategory foodCategory)
        {
            throw new NotImplementedException();
        }

        public void Update(FoodCategory foodCategory)
        {
            throw new NotImplementedException();
        }
    }
}
