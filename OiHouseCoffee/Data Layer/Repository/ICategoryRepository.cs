﻿using BusinessLayer.Models;
using DataLayer.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OiHouseCoffee.Data_Layer.Repository
{
    public interface ICategoryRepository
    {
        IEnumerable<FoodCategory> GetAll();
        void Add(FoodCategory foodCategory);
        void Update(FoodCategory foodCategory);
        void Remove(FoodCategory foodCategory);
        Food FindById(int id);
    }
}
