﻿using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OiHouseCoffee.Data_Layer.Repository
{
    internal class OrderInfoRepository : IOrderInfoRepository
    {

        public void Add(BillInfo billInfo)
        {
             OrderInfoDao.Instance.Add(billInfo);
        }

        public IEnumerable<BillInfo> FindAllById(int id)
        {
            return OrderInfoDao.Instance.GetAllById(id).ToList();
        }
    }
}
