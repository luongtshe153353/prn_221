﻿using BusinessLayer.Models;
using DataLayer;
using DataLayer.Filter;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;


namespace OiHouseCoffee.Data_Layer.Repository
{
    
    internal class AccountRepository : IAccountRepository
    {
        public void Add(Account account)
        {
            AccountDAO.Instance.Add(account);
        }

        public void Delete(Account account)
        {
            AccountDAO.Instance.Delete(account);
        }

        public IEnumerable<Account> FindAllBy(AccountFilter filter)
        {
            if (filter != null)
            {
                return AccountDAO.Instance.FindAll(account => (filter.AccountId == null || account.Id.Equals(filter.AccountId)) &&
                                                              (filter.Email == null || account.Email.ToLower().Contains(filter.Email.ToLower())) &&
                                                              (filter.UserName == null || account.UserName.ToLower().Contains(filter.UserName.ToLower())) &&
                                                              (filter.PhoneNumber == null || account.PhoneNumber.Contains(filter.PhoneNumber)) && account.Status == true);
            }
            return GetAll();
        }

        public Account FindByEmail(string email)
        {
            return AccountDAO.Instance.FindOne(account => account.Email == email);
        }

        public Account FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Account FindByName(string name)
        {
            return AccountDAO.Instance.FindOne(account => account.UserName == name);
        }

        public Account FindByPasswordAndUserName(string password, string username)
        {
            return AccountDAO.Instance.FindOne(Account => Account.UserName == username && Account.PassWord == password);
        }

        public Account FindByPhoneNumber(string phoneNumber)
        {
            return AccountDAO.Instance.FindOne(account => account.PhoneNumber == phoneNumber);
        }

        public IEnumerable<Account> GetAll()
        {
            return AccountDAO.Instance.List();
        }

        public void Update(Account account)
        {
            AccountDAO.Instance.Update(account);
        }
    }
}
