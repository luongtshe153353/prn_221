﻿using BusinessLayer.Models;
using DataLayer.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OiHouseCoffee.Data_Layer.Repository
{
    internal class BillRepository : IBillRepository
    {
        public void Add(Bill bill)
        {
            BillDAO.Instance.Add(bill);
        }

        public void Delete(Bill bill)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Bill> FindAllBy(BillFilter filter)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Bill> FindAllByAccount(string username)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Bill> FindAllByStartTimeAndEndTime(BillFilter billFilter)
        {
           
             return BillDAO.Instance.FindAll(order => (billFilter.StartDate == null || order.DateCheckOut >= order.DateCheckOut) &&
                                                       (billFilter.EndDate == null || order.DateCheckOut <= order.DateCheckOut ) ||
                                                        (billFilter.StartDate != null && billFilter.EndDate != null 
                                                        && order.DateCheckOut >= billFilter.StartDate && order.DateCheckOut <= billFilter.EndDate))
                                                        .OrderByDescending(order => order.DateCheckOut).ToList();
        }

        public Bill Get(int id)
        {
            throw new NotImplementedException();
        }



        //public IEnumerable<Bill> GetLastProduct() {
        //    Bill bill = new Bill();
        //    return bill;
        //}

        public IEnumerable<Bill> GetAll()
        {
            return BillDAO.Instance.List();
        }

        public void Update(Bill bill)
        {
            throw new NotImplementedException();
        }
    }
}
