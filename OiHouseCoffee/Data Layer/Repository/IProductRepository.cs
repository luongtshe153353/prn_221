﻿
using BusinessLayer.Models;
using DataLayer.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OiHouseCoffee.Data_Layer.Repository
{
    public interface IProductRepository
    {
        IEnumerable<Food> List();
        void Add(Food Food);
        void Update(Food Food);
        void Remove(Food Food);
        Food FindById(int id);
        Food FindByName(string name);
        Food FintByCategory(int category);
        IEnumerable<Food> FindByPrice(int price);
        IEnumerable<Food> FindAllBy(ProductFilter filter);
    }
}
