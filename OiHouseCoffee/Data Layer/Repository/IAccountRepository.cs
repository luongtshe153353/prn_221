﻿using DataLayer.Filter;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OiHouseCoffee.Data_Layer.Repository
{
    public interface IAccountRepository
    {
        IEnumerable<Account> GetAll();
        void Add(Account account);
        void Update(Account account);
        void Delete(Account account);
        Account FindById(int id);
        Account FindByName(string name);

        Account FindByPhoneNumber(string name);

        Account FindByEmail(string name);
        Account FindByPasswordAndUserName(string username,string password);

        IEnumerable<Account> FindAllBy(AccountFilter filter);
    }
}
