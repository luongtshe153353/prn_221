﻿using BusinessLayer.Models;
using DataLayer.Filter;
using Microsoft.Win32;
using OiHouseCoffee.Data_Layer.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for AddEmployee.xaml
    /// </summary>
    public partial class AddEmployee : Window
    {
        private readonly IAccountRepository accountRepository;
        private readonly EmployeeManeger employeeManeger;
        private Account? account;
        private ListView listView;
        private static readonly Regex _regex = new Regex("[^0-9.-]+");
        public AddEmployee(EmployeeManeger _employeeManeger, Account? _account, IAccountRepository _accountRepository, ListView _listView)
        {
            InitializeComponent();
            this.accountRepository = _accountRepository;
            this.employeeManeger = _employeeManeger;
            this.account = _account;
            this.listView = _listView;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (account != null)
            {
                txtBoxEmail.Text = account.Email;
                txtBoxUserName.Text = account.UserName;
                txtBoxPassWord.Text = account.PassWord;
                txtBoxPhoneNumber.Text = account.PassWord;
                txtBoxId.Text = account.Id.ToString();
                txtBoxId.Visibility = Visibility.Visible;
                labelId.Visibility = Visibility.Visible;
                btn.Content = "Update";
                this.Height = 550;
            }
        }
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            string email = txtBoxEmail.Text;
            string userName = txtBoxUserName.Text;
            string phoneNumber = txtBoxPhoneNumber.Text;
            string password = txtBoxPassWord.Text;
            Account accountFindByEmail = accountRepository.FindByEmail(email);
            Account accountFindByPhone = accountRepository.FindByPhoneNumber(phoneNumber);
            Account accountFindByUserName = accountRepository.FindByName(userName);
            string avatarPath;
            if (imageProduct.Tag != null)
            {
                avatarPath = Path.GetFullPath(imageProduct.Tag.ToString());
            }
            else
            {
                avatarPath = @"Images\Logo.JPG";
            }
            String msg = null;
            Account? p = null;
            if (account != null)
            {
                p = account;
            }
            else
            {
                p = new Account();
            }
            p.Email = email;
            p.UserName = userName;
            p.PhoneNumber = phoneNumber;
            p.PassWord = password;
            p.Avatar = avatarPath;
            if (account != null)
            {

                if (accountFindByEmail != null && !account.Id.Equals(accountFindByEmail.Id))
                {
                    msg += "Email";
                }
                if (accountFindByPhone != null && !account.Id.Equals(accountFindByPhone.Id))
                {
                    msg += ", Phone Number";
                }
                if (accountFindByUserName != null && !account.Id.Equals(accountFindByUserName.Id))
                {
                    msg += ", UserName";
                }
                if (accountFindByEmail != null || accountFindByPhone != null || accountFindByUserName != null)
                {
                    MessageBox.Show(msg + " đã tồn tại!");
                }
                else
                {
                    accountRepository.Update(p);
                    listView.ItemsSource = accountRepository.GetAll();
                    this.Close();
                }
                
            }
            else
            {
                
                
                if (accountFindByEmail != null)
                {
                    msg += "Email";
                }
                if(accountFindByPhone != null)
                {
                    msg += ", Phone Number";
                }
                if(accountFindByUserName != null)
                {
                    msg += ", UserName";
                }
                if(accountFindByEmail != null || accountFindByPhone != null || accountFindByUserName != null)
                {
                    MessageBox.Show(msg + " đã tồn tại!");
                }
                else
                {
                    accountRepository.Add(p);
                    listView.ItemsSource = accountRepository.GetAll();
                    this.Close();
                }
               
            }
            
        }

        private void txtBoxPhoneNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_regex.IsMatch(txtBoxPhoneNumber.Text))
            {
                MessageBox.Show("Plase input number");
                txtBoxPhoneNumber.Text = string.Empty;
            }
        }

        private void btn_Click1(object sender, RoutedEventArgs e)
        {
            //Create a new instance of openFileDialog
            OpenFileDialog image = new OpenFileDialog();
            //Filter
            image.Filter = "Image Files|*.jpg;*.jpeg;*.png;*.tif;...";

            //When the user select the file
            if (image.ShowDialog() == true)
            {
                //Get the file's path
                string imgPath = image.FileName;
                //Do something
                ImageSourceConverter imgsc = new ImageSourceConverter();
                ImageSource imageSource = (ImageSource)imgsc.ConvertFromString(imgPath);
                imageProduct.Tag = imgPath;
                imageProduct.Source = imageSource;
            }
        }
    }
}
