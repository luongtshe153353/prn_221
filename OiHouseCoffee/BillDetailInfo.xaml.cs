﻿using BusinessLayer.Models;
using OiHouseCoffee.Data_Layer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for BillDetailInfo.xaml
    /// </summary>
    public partial class BillDetailInfo : Window
    {
        private readonly IOrderInfoRepository orderInfoRepository;
        private Bill bill;

        public BillDetailInfo(Bill _bill, IOrderInfoRepository _orderInfoRepository)
        {
            orderInfoRepository = _orderInfoRepository;
            bill = _bill;
            InitializeComponent();
            setUp();
        }

        public void setUp()
        {
            List<BillInfo> billInfos = (List<BillInfo>)orderInfoRepository.FindAllById(bill.Id);
            txtAccount.Text = bill.IdAccountNavigation.UserName;
            timeCheckIn.Text = DateTime.Now.ToString();
            timeCheckOut.Text = DateTime.Now.ToString();
            int totalBeforDiscount = 0;
            int discount = 0;
            int totalAfterDiscount = 0;
            foreach (BillInfo billinfo in billInfos)
            {
                billinfo.price = billinfo.Count * billinfo.IdFoodNavigation.Price;
                totalBeforDiscount += billinfo.Count * billinfo.IdFoodNavigation.Price;
            }
            discount = (int)bill.Discount;
            TotalBeforDiscount.Text = totalBeforDiscount.ToString();
            if(discount > 0)
            {
                totalAfterDiscount = totalBeforDiscount * discount / 100;
            }
            else
            {
                totalAfterDiscount = totalBeforDiscount;
            }
           
            PercentDiscount.Text = discount.ToString();
            TotalAfterDiscount.Text = totalAfterDiscount.ToString();
            Discount.Text = (totalAfterDiscount - totalBeforDiscount).ToString();
            ListProduct.ItemsSource = billInfos;
        }
    }
}
