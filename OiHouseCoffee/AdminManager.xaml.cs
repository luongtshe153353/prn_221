﻿
using OiHouseCoffee.Data_Layer.Repository;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for AdminManager.xaml
    /// </summary>
    public partial class AdminManager : Window
    {
        private readonly IProductRepository productRepository;
        private readonly IAccountRepository accountRepository;
        private readonly ICategoryRepository categoryRepository;
        private readonly IBillRepository billRepository;
        private readonly IOrderInfoRepository orderInfoRepository;
        private readonly MainWindow mainWindow;
        public AdminManager(MainWindow _mainWindow, IProductRepository _productRepository, IAccountRepository _accountRepository, 
                            IBillRepository _billRepository, ICategoryRepository _categoryRepository, IOrderInfoRepository _orderInfoRepository)
        {
            InitializeComponent();
            this.productRepository = _productRepository;
            this.accountRepository = _accountRepository;
            this.billRepository = _billRepository;
            this.categoryRepository = _categoryRepository;
            this.mainWindow = _mainWindow;
            Closing += AdminManager_Closing;
            this.orderInfoRepository = _orderInfoRepository;
        }
        private void AdminManager_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            mainWindow.Show();
        }
        private void Button_Logout(object sender, RoutedEventArgs e)
        {
            this.Close();
            mainWindow.Show();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(e.ChangedButton == MouseButton.Left)
            {
                this.DragMove();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            EmployeeManeger employeeManeger = new EmployeeManeger(accountRepository);
            frameMain.Content = employeeManeger;
            setButtonColor(0);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ProductManager productManager = new ProductManager(productRepository,categoryRepository);
            frameMain.Content = productManager;
            setButtonColor(1);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {   
            
            BillManager billManager = new BillManager(billRepository,orderInfoRepository);
            frameMain.Content = billManager;
            setButtonColor(2);
        }

        public void setButtonColor(int numberButton)
        {
            if(numberButton == 0)

            {
                buttonEmployee.Background = (SolidColorBrush)new BrushConverter().ConvertFrom("#02be68");
                buttonProduct.Background = null;
                buttonBill.Background = null;
                buttonEmployee.Foreground = new SolidColorBrush(Colors.White);
                buttonProduct.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFCFCFCF");
                buttonBill.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFCFCFCF");
            }
            else if(numberButton == 1)
            {
                buttonProduct.Background = (SolidColorBrush)new BrushConverter().ConvertFrom("#02be68");
                buttonEmployee.Background = null;
                buttonBill.Background = null;
                buttonProduct.Foreground = new SolidColorBrush(Colors.White);
                buttonEmployee.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFCFCFCF");
                buttonBill.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFCFCFCF");
            }
            else
            {
                buttonBill.Background =  (SolidColorBrush)new BrushConverter().ConvertFrom("#02be68");
                buttonProduct.Background = null;
                buttonEmployee.Background = null;
                buttonBill.Foreground = new SolidColorBrush(Colors.White);
                buttonEmployee.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFCFCFCF");
                buttonProduct.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFCFCFCF");
            }
        }
    }
}
